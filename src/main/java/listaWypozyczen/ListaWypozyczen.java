package listaWypozyczen;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ksiazka.Ksiazka;
import ksiazka.ListaKsiazek;
import narzedzia.Narzedzia;
import uzytkownik.ListaUzytkownikow;
import uzytkownik.Uzytkownik;

import java.io.*;
import java.sql.ClientInfoStatus;
import java.util.*;
import java.util.stream.Collectors;

public class ListaWypozyczen {
    transient private HashMap<Uzytkownik, ListaKsiazek> listaWypozyczen = new HashMap<Uzytkownik, ListaKsiazek>();
    transient private Uzytkownik zalogowanyUzytkownik;
    transient private ListaUzytkownikow listaUzytkownikow;
    transient private ListaKsiazek listaKsiazek;
    private HashMap<String, List<String>> listaWypozyczenDoSerializacji = new HashMap<String, List<String>>();

    public ListaWypozyczen(Uzytkownik zalogowanyUzytkownik) {
        this.zalogowanyUzytkownik = zalogowanyUzytkownik;
    }

    public ListaWypozyczen(Uzytkownik zalogowanyUzytkownik, ListaUzytkownikow listaUzytkownikow, ListaKsiazek listaKsiazek) {
        this.zalogowanyUzytkownik = zalogowanyUzytkownik;
        this.listaUzytkownikow = listaUzytkownikow;
        this.listaKsiazek = listaKsiazek;
    }

    public ListaWypozyczen() {
    }

    public ListaKsiazek get(Uzytkownik uzytkownik) {
        return listaWypozyczen.get(uzytkownik);
    }

    public void wypozyczKsiazke(Ksiazka ksiazka) {
        ListaKsiazek listaWypozyczoncyhKsiazek;

        if (listaWypozyczen.containsKey(zalogowanyUzytkownik)) {
            listaWypozyczoncyhKsiazek = listaWypozyczen.get(zalogowanyUzytkownik);
            if (listaWypozyczoncyhKsiazek == null) {
                listaWypozyczoncyhKsiazek = new ListaKsiazek();
            }
        } else {
            listaWypozyczoncyhKsiazek = new ListaKsiazek();
            listaWypozyczen.put(zalogowanyUzytkownik, listaWypozyczoncyhKsiazek);
        }
        listaWypozyczoncyhKsiazek.dodajKsiazke(ksiazka);
        ksiazka.wypozyczKsiazke(zalogowanyUzytkownik);
        Narzedzia.wait((double) 1);
    }

    public void zwrocKsiazke(Ksiazka ksiazka) {
        ksiazka.zwrocKsiazke();
        listaWypozyczen.get(zalogowanyUzytkownik).usunKsiazke(ksiazka);
        Narzedzia.wait((double) 1);
    }

    public boolean zapisDoPliku(String scierzkaDoPliku) {
        boolean udaloSie;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        wygenerujListaWypozyczenDoSerializacji();
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(scierzkaDoPliku));
            bufferedWriter.write(gson.toJson(this));
            bufferedWriter.close();
            udaloSie = true;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Błąd podczas próby zapisy listy książek do pliku");
            udaloSie = false;
        }
        return udaloSie;
    }

    private void wygenerujListaWypozyczenDoSerializacji() {
        if (!listaWypozyczen.isEmpty()) {
            Set<Uzytkownik> setUzytkownikow = listaWypozyczen.keySet();
            listaWypozyczenDoSerializacji = new HashMap<String, List<String>>();
            for (Uzytkownik user : setUzytkownikow) {
                listaWypozyczenDoSerializacji.put(user.getNumerBibloteczny()
                        , listaWypozyczen.get(user)
                                .getListaKsiazek()
                                .stream()
                                .map(k -> k.getNrBibliotecznyKsiazki())
                                .collect(Collectors.toList())
                );
            }
        } else {
            System.out.println("Zapis listy wypożyczeń nie powiódł się. Lista wypożyczeń jest pusta.");
        }
    }

    public boolean odczytZPliku(String scierzkaDoPliku) {
        this.listaKsiazek = listaKsiazek;
        this.listaUzytkownikow = listaUzytkownikow;
        boolean udaloSie;
        Gson gson = new Gson();
        BufferedReader bufferedReader;

        try {
            bufferedReader = new BufferedReader(new FileReader(scierzkaDoPliku));
            ListaWypozyczen listaWypozyczenTemp = gson.fromJson(bufferedReader, ListaWypozyczen.class);
            boolean czyListaJestPusta = sprawdzCzyListaWypozyczenJestPusta(listaWypozyczenTemp);
            if (!czyListaJestPusta) {
                listaWypozyczen = dodajWypozyczeniaOdczytaneZPliku(listaWypozyczenTemp);
            } else {
                listaWypozyczen = new HashMap<Uzytkownik, ListaKsiazek>();
                System.out.println("Plik nie zawiera wypożyczeń.");
            }
            bufferedReader.close();
            udaloSie = true;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Plik z listą książek nie istnieje, lub nie można go otworzyć");
            udaloSie = false;
        }
        return udaloSie;
    }

    public boolean odczytZPliku(String scierzkaDoPliku, ListaKsiazek listaKsiazek, ListaUzytkownikow listaUzytkownikow) {
        this.listaKsiazek = listaKsiazek;
        this.listaUzytkownikow = listaUzytkownikow;
        boolean udaloSie;
        Gson gson = new Gson();
        BufferedReader bufferedReader;

        try {
            bufferedReader = new BufferedReader(new FileReader(scierzkaDoPliku));
            ListaWypozyczen listaWypozyczenTemp = gson.fromJson(bufferedReader, ListaWypozyczen.class);
            boolean czyListaJestPusta = sprawdzCzyListaWypozyczenJestPusta(listaWypozyczenTemp);
            if (!czyListaJestPusta) {
                listaWypozyczen = dodajWypozyczeniaOdczytaneZPliku(listaWypozyczenTemp);
            } else {
                listaWypozyczen = new HashMap<Uzytkownik, ListaKsiazek>();
                System.out.println("Plik nie zawiera wypożyczeń.");
            }
            bufferedReader.close();
            udaloSie = true;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Plik z listą książek nie istnieje, lub nie można go otworzyć");
            udaloSie = false;
        }
        return udaloSie;
    }

    private boolean sprawdzCzyListaWypozyczenJestPusta(ListaWypozyczen listaWypozyczenTemp) {
        boolean czyListaWypozyczenJestPusta = true;
        if (listaWypozyczenTemp != null) {
            if (listaWypozyczenTemp.getListaWypozyczenDoSerializacji() != null) {
                if (!listaWypozyczenTemp.getListaWypozyczenDoSerializacji().isEmpty()) {
                    if(listaWypozyczenTemp.getListaWypozyczenDoSerializacji().keySet() != null){
                        if(!listaWypozyczenTemp.getListaWypozyczenDoSerializacji().keySet().isEmpty()){
                            czyListaWypozyczenJestPusta = false;
                        }
                    }
                }
            }
        }
        return czyListaWypozyczenJestPusta;
    }

    private HashMap<Uzytkownik, ListaKsiazek> dodajWypozyczeniaOdczytaneZPliku(ListaWypozyczen listaWypozyczenTemp) {
        HashMap<Uzytkownik, ListaKsiazek> wczytanaIRozwiazanaListaKsiazek = new HashMap<>();
        ListaKsiazek listaKsiazekTemp = new ListaKsiazek();

        HashMap<String, List<String>> listaWypozyczenDoSerializacjiTemp = listaWypozyczenTemp.getListaWypozyczenDoSerializacji();
        Set<String> setUzytkownikowTemp = listaWypozyczenDoSerializacjiTemp.keySet();
        List<String> listaNumerowBibliotecznychKsiazekWypozyczonychPrzezDanegoUzytkownika;
        int iloscUzytkownikow = setUzytkownikowTemp.size();
        Optional<Uzytkownik> uzytkownikTempOptional = Optional.empty();
        Uzytkownik uzytkownikTemp = null;
        Optional<Ksiazka> ksiazkaTemp = Optional.empty();

        List<String> numeryBiblioteczneUzytkownikowKtorychNieUdaloSieOdnalezc = null;
        int licznikNieznalezionychUzytkownikow = 0;

        for (String userId : setUzytkownikowTemp) {
            uzytkownikTempOptional = listaUzytkownikow.znajdzUzytkownika(null, userId);

            if (uzytkownikTempOptional.isPresent()) {
                uzytkownikTemp = uzytkownikTempOptional.get();
            } else {
                licznikNieznalezionychUzytkownikow++;
                numeryBiblioteczneUzytkownikowKtorychNieUdaloSieOdnalezc.add(userId);
            }

            listaNumerowBibliotecznychKsiazekWypozyczonychPrzezDanegoUzytkownika = listaWypozyczenDoSerializacjiTemp.get(userId);
            if (listaNumerowBibliotecznychKsiazekWypozyczonychPrzezDanegoUzytkownika != null) {
                if (!listaNumerowBibliotecznychKsiazekWypozyczonychPrzezDanegoUzytkownika.isEmpty()) {
                    for (String bookId : listaNumerowBibliotecznychKsiazekWypozyczonychPrzezDanegoUzytkownika) {
                        ksiazkaTemp = listaKsiazek.znajdzKsiazke(bookId);
                        if (ksiazkaTemp.isPresent()) {
                            listaKsiazekTemp.dodajKsiazke(ksiazkaTemp.get());
                        }
                        if (uzytkownikTemp != null) {
                            wczytanaIRozwiazanaListaKsiazek.put(uzytkownikTemp, listaKsiazekTemp);
                        }
                    }
                }
            }
        }

        if (licznikNieznalezionychUzytkownikow == 0) {
            System.out.println("Odczyt listy wypożyczeń z pliku powiódł się w 100%.");
            System.out.println("Wczytane wypożyczenia " + iloscUzytkownikow + " użytkowników.");
        } else {
            System.out.println("Odczyt listy wypożyczeń z pliku nie powiódł się dla " + licznikNieznalezionychUzytkownikow + " użytkowników.");
            System.out.println("Lista numerów bibliotecznych użytkowników, których nie udało się odnaleźć:");
            for (String userId : numeryBiblioteczneUzytkownikowKtorychNieUdaloSieOdnalezc) {
                System.out.println(userId);
            }
        }
        return wczytanaIRozwiazanaListaKsiazek;
    }

    public HashMap<Uzytkownik, ListaKsiazek> getListaWypozyczen() {
        return listaWypozyczen;
    }

    private HashMap<String, List<String>> getListaWypozyczenDoSerializacji() {
        return listaWypozyczenDoSerializacji;
    }

    public void setZalogowanyUzytkownik(Uzytkownik zalogowanyUzytkownik) {
        this.zalogowanyUzytkownik = zalogowanyUzytkownik;
    }

    @Override
    public String toString(){
        String string = null;
        Set<Uzytkownik> listaUzytkownikow = listaWypozyczen.keySet();
        for (Uzytkownik uzytkownik:listaUzytkownikow) {
            string = string + uzytkownik + listaWypozyczen.get(uzytkownik);
        }
        return string;
    }
}
