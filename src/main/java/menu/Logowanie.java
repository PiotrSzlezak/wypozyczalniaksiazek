package menu;


import ksiazka.ListaKsiazek;
import listaWypozyczen.ListaWypozyczen;
import narzedzia.Narzedzia;
import uzytkownik.ListaUzytkownikow;
import uzytkownik.PeselValidator;
import uzytkownik.PoziomUprawnien;
import uzytkownik.Uzytkownik;

import java.util.Optional;
import java.util.Scanner;

public class Logowanie {
    private ListaKsiazek listaKsiazek;
    private ListaUzytkownikow listaUzytkownikow;
    Scanner sc = new Scanner(System.in);
    private Uzytkownik uzytkownik;
    private int menu;
    private ListaWypozyczen listaWypozyczen;

    public Logowanie(ListaKsiazek listaKsiazek, ListaUzytkownikow listaUzytkownikow, ListaWypozyczen listaWypozyczen) {
        this.listaKsiazek = listaKsiazek;
        this.listaUzytkownikow = listaUzytkownikow;
        this.listaWypozyczen = listaWypozyczen;
    }


    public void wyszukajUzytkownika() {
        System.out.println("Dzień dobry,\nWyszukaj użytkownika:" +
                "\n 1.Wyszukaj po PESEL-u." +
                "\n 2.Wyszukaj po Numerze Karty Bibliotecznej." +
                "\n 3.Wyjdź z programu.");

        menu = Narzedzia.sprawdzWprowadznaWartoscMenu(1, 3);
        if (menu == 1) {
            boolean notValid;
            String pesel;
            do {
                System.out.println("Podaj Pesel:");
                pesel = sc.next();
                notValid = !PeselValidator.valid(pesel);
            } while (notValid);
            Optional<Uzytkownik> uzytkownik = listaUzytkownikow.znajdzUzytkownika(pesel, null);
            if (uzytkownik.isEmpty()) {
                logowanieNowyUzytkownik(pesel, null);
            }
            if (uzytkownik.isPresent()) {
                uzOdnalezionyCoRobic(uzytkownik);
                wyszukajUzytkownika();
            }
        }
        if (menu == 2) {
            System.out.println("Podaj Numer Karty Biblotecznej(np.KB-11111):");
            String nrKarty = sc.next();
            listaUzytkownikow.znajdzUzytkownika(null, nrKarty);
            Optional<Uzytkownik> uzytkownik = listaUzytkownikow.znajdzUzytkownika(null, nrKarty);
            if (uzytkownik.isEmpty()) {
                logowanieNowyUzytkownik(null, nrKarty);
            }
            if (uzytkownik.isPresent()) {
                uzOdnalezionyCoRobic(uzytkownik);
            }
        }
//        if (menu == 3) {
//        }
    }

    public void logowanieNowyUzytkownik(String pesel, String nrKarty) {

        System.out.println("Nie znaleźono użytkownika. Co chcesz zrobić? \n1. Utwórz nowego użytkowniaka. " +
                "\n2. Powrót do menu głównego.");
        menu = Narzedzia.sprawdzWprowadznaWartoscMenu(1, 2);
        if (menu == 1) {
            System.out.println("Tworzenie nowego użytkownika.");
            System.out.println("Podaj imię:");
            String noImie = sc.next();
            System.out.println("Podaj nazwisko:");
            String noNazwisko = sc.next();
            String noPesel;
            if (pesel == null) {
                boolean notValid;
                String pesel1;
                do {
                    System.out.println("Podaj Pesel:");
                    pesel1 = sc.next();
                    notValid = !PeselValidator.valid(pesel1);
                } while (notValid);
                noPesel = pesel1;
            } else
                noPesel = pesel;
            System.out.println("Podaj e-mail:");
            String noEmail = sc.next();
            Uzytkownik nowyUzytkownik = new Uzytkownik(noImie, noNazwisko, noPesel, noEmail);
            listaUzytkownikow.dodajUzytkowniaka(nowyUzytkownik);
            uzytkownik = nowyUzytkownik;
            uruchomMenu();
        }
        if (menu == 2) {
            wyszukajUzytkownika();
        }
    }

    private void uruchomMenu() {
        listaWypozyczen.setZalogowanyUzytkownik(uzytkownik);
        if (uzytkownik.getPoziomUprawnien().equals(PoziomUprawnien.ADMIN)) {
            MenuAdmina menuAdmina = new MenuAdmina(uzytkownik, listaKsiazek, listaWypozyczen, listaUzytkownikow);
            menuAdmina.uruchomMenu();
        } else {
            MenuKlienta menuKlienta = new MenuKlienta(uzytkownik, listaKsiazek, listaWypozyczen);
            menuKlienta.uruchomMenu();
        }
    }

    public void uzOdnalezionyCoRobic(Optional<Uzytkownik> uzytkownikOptional) {
        System.out.println("Użytkownik znaleziony - " + uzytkownikOptional.get().getImie() + " " + uzytkownikOptional.get().getNazwisko()
                + " | Numer Karty Bibliotecznej: " + uzytkownikOptional.get().getNumerBibloteczny() +
                "\n1.Modyfikuj użytkownika.\n2.Przejdź do wypożyczenia.\n3.Wróć do wyszukiwania użytkownika.");
        menu = Narzedzia.sprawdzWprowadznaWartoscMenu(1, 3);
        switch (menu) {
            case 1:
                opcjeUzytkownika(uzytkownikOptional.get());
                break;
            case 2:
                this.uzytkownik = uzytkownikOptional.get();
                uruchomMenu();
                break;
            case 3:
                wyszukajUzytkownika();
                break;
        }
    }

    private void opcjeUzytkownika(Uzytkownik uzytkownik) {
        System.out.println("Modyfikacja użytkownika:\n1.Modyfikacja danych(Imię/Nazwisko/E-mail)\n2.Usuń użytkownika" +
                "\n3.Wróć do wyszukiwania użytkownika.\n4.Nadaj uprawnienia administratorskie.");
        menu = Narzedzia.sprawdzWprowadznaWartoscMenu(1, 4);
        switch (menu) {
            case 1:
                boolean uz1 = listaUzytkownikow.modyfikujUzytkownika(uzytkownik);
                if (uz1) {
                    System.out.println("Użytkownik zmodyfikowany.");
                    opcjeUzytkownika(uzytkownik);
                } else
                    System.out.println("Nie zmieniono danych użytkownika.");
                    opcjeUzytkownika(uzytkownik);
                break;
            case 2:
                System.out.println("Czy chcesz usunać użytkownika?\n1.Tak\n2.Nie");
                int menu2 = Narzedzia.sprawdzWprowadznaWartoscMenu(1, 2);
                switch (menu2) {
                    case 1:
                        listaUzytkownikow.usunUzytkowniaka(uzytkownik);
                        break;
                    case 2:
                        wyszukajUzytkownika();
                        break;
                }
            case 3:
                wyszukajUzytkownika();
                break;
            case 4:
                nadajUprawnieniaAdministratorskie(uzytkownik);
                break;
        }
    }

    private void nadajUprawnieniaAdministratorskie(Uzytkownik uzytkownik) {
        System.out.println("Aby nadać temu użytkownika uprawnienia administratorskie, musisz podać hasło administratora.");
        System.out.println("Podaj hasło administratora:");
        String haslo;
        boolean warunekPetli;
        do {
            haslo = sc.next();
            if (uzytkownik.nadajUprawnieniaAdministratorskie(haslo)) {
                System.out.println("Użytkownik " + uzytkownik.getImie() + " " + uzytkownik.getNazwisko() + " otrzymał uprawnienia administratorskie.");
                warunekPetli = false;
            } else {
                System.out.println("Podałeś błędne hasło. Spróbuj jeszcze raz.");
                warunekPetli = true;
            }
        } while (warunekPetli);
        wyszukajUzytkownika();
    }
}
