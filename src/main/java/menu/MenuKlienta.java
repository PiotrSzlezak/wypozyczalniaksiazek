package menu;

import ksiazka.Ksiazka;
import ksiazka.ListaKsiazek;
import listaWypozyczen.ListaWypozyczen;
import narzedzia.Narzedzia;
import uzytkownik.Uzytkownik;
import wysylkaPowiadomien.WyslkaEmail;

import java.io.*;
import java.util.Objects;
import java.util.Optional;

public class MenuKlienta {

    private Uzytkownik zalogowanyUzytkownik;
    private BufferedReader br;
    private ListaKsiazek listaKsiazek;
    private ListaWypozyczen listaWypozyczen;

    public MenuKlienta(Uzytkownik zalogowanyUzytkownik, ListaKsiazek listaKsiazek, ListaWypozyczen listaWypozyczen) {
        this.zalogowanyUzytkownik = zalogowanyUzytkownik;
        this.listaKsiazek = listaKsiazek;
        br = new BufferedReader(new InputStreamReader(System.in));
        if(listaWypozyczen!=null){
            this.listaWypozyczen = listaWypozyczen;
        } else {
            this.listaWypozyczen = new ListaWypozyczen();
        }
    }

    public void uruchomMenu() {
        Narzedzia.wait((double) 1);
        System.out.println("\nCześć " + zalogowanyUzytkownik.getImie() + "!");
        menuGlowne();
    }

    private void menuGlowne() {
        Narzedzia.pressAnyKeyToContinue();
        System.out.println("\nWprowadź numer akcji, którą chcesz wykonać:");
        System.out.println("1. Wypożycz książkę.");
        System.out.println("2. Zwróć książkę.");
        System.out.println("3. Sprawdź listę wypożyczonych książek.");
        System.out.println("4. Znajdź książkę, która Cię interesuje.");
        System.out.println("5. Wyloguj się.");

        int wybranaOpcja = Narzedzia.sprawdzWprowadznaWartoscMenu(1, 5);
        switch (wybranaOpcja) {
            case 1:
                wypozyczKsiazkeMenu();
                break;
            case 2:
                zwrocKsiazkeMenu();
                break;
            case 3:
                sprawdzListeWypozyczonychKsiazekMenu();
                break;
            case 4:
                znajdzKsiazkiKtoraCieInteresujaMenu();
                break;
            case 5:
                break;
        }
    }

    private void wypozyczKsiazkeMenu() {
        System.out.println("\nWypożyczanie książek.");
        System.out.println("Wprowadź numer biblioteczny książki(np. ABC-1111), którą chcesz wypożyczyć:");
        String numerBiblioteczny = "";

        boolean warunekPetli;
        Optional<Ksiazka> ksiazkaOptional;

        do {
            String szukanyNumerBiblioteczny = readLine();
            ksiazkaOptional = listaKsiazek.znajdzKsiazke(szukanyNumerBiblioteczny);
            if (ksiazkaOptional.isEmpty()) {
                System.out.println("Książka o numerze bibliotecznym " + szukanyNumerBiblioteczny + " nie istnieje w naszej bazie danych. Być może podałeś niepoprawny numer.\nSpróbuj jeszcze raz.");
                warunekPetli = true;
            } else if (ksiazkaOptional.get().getCzyKsiazkaJestWypozyczona()) {
                System.out.println("Książka o numerze bibliotecznym " + szukanyNumerBiblioteczny + " jest już wypożyczona. Wybierz inną książkę i podaj jej numer biblioteczny.");
                warunekPetli = true;
            } else {
                warunekPetli = false;
            }
        } while (warunekPetli);

        listaWypozyczen.wypozyczKsiazke(ksiazkaOptional.get());
        System.out.println("Gratulacje! Wypożyczyłeś książkę " + ksiazkaOptional.get().getTytulKsiazki());
        System.out.println("Zwróć ją proszę w ciągu 3 tygodni(do " + ksiazkaOptional.get().getDataZwrotu() + ").");
        WyslkaEmail wyslkaEmail = new WyslkaEmail();
        wyslkaEmail.wyslkaEmailAsync(zalogowanyUzytkownik.getEmail(),
                "Wypożyczalnia książek - Informacja o wypożyczonej książce",
                "Witaj " + zalogowanyUzytkownik.getImie() + "," +
                        "\n\nWłaśnie została przez Ciebie wypożyczona książka: " + ksiazkaOptional.get().getTytulKsiazki() +
                        "\n\nKsiążka została wypożyczona w dniu " + ksiazkaOptional.get().getAktualnaData() +"."+
                        "\nPamiętaj by zwrócić książkę do: " + ksiazkaOptional.get().getDataZwrotu() +
                        "\nMiłego czytania :)" + "\n\nPozdrawiamy,\nZespół Wypożyczalnia Książek");
        menuGlowne();
    }

    private void zwrocKsiazkeMenu(){
        System.out.println("\nZwroty książek.");
        System.out.println("Wprowadź numer biblioteczny książki, którą chcesz zwrócić:");
        String numerBiblioteczny = "";

        boolean warunekPetli = false;
        Optional<Ksiazka> ksiazkaOptional;

        do {
            String szukanyNumerBiblioteczny = readLine();
            ksiazkaOptional = listaKsiazek.znajdzKsiazke(szukanyNumerBiblioteczny);
            if (ksiazkaOptional.isEmpty()) {
                System.out.println("Książka o numerze bibliotecznym "+szukanyNumerBiblioteczny+" nie istnieje w naszej bazie danych. Być może podałeś niepoprawny numer.\nSpróbuj jeszcze raz.");
                warunekPetli = true;
            } else if (!ksiazkaOptional.get().getCzyKsiazkaJestWypozyczona()) {
                System.out.println("Książka o numerze bibliotecznym "+szukanyNumerBiblioteczny+" nie jest wypożyczona. Być może podałeś niepoprawny numer.\nSpróbuj jeszcze raz.");
                warunekPetli = true;
            } else if(listaWypozyczen.get(zalogowanyUzytkownik).znajdzKsiazke(szukanyNumerBiblioteczny).isEmpty()){
                System.out.println("Książka o numerze bibliotecznym "+szukanyNumerBiblioteczny+" nie jest wypożyczona przez Ciebie. Czy chcesz ją zwrócić w czyimś imieniu?");
                System.out.println("1. Tak\n2. Nie");

                int wybranaOpcja = Narzedzia.sprawdzWprowadznaWartoscMenu(1,2);
                switch (wybranaOpcja){
                    case 1:

                        Uzytkownik innyUzytkownik = ksiazkaOptional.get().getUzytkownikObecniePosiadajacyTaKsiazke();
                        listaWypozyczen.zwrocKsiazke(ksiazkaOptional.get());
                        System.out.println("Książka "+ksiazkaOptional.get().getTytulKsiazki()+" wypożyczona przez "+innyUzytkownik.getImie()+" "+innyUzytkownik.getNazwisko()+" została zwrócona przez "+zalogowanyUzytkownik.getImie()+" "+zalogowanyUzytkownik.getNazwisko()+".");
                        break;
                    case 2:
                        break;
                }

            } else {
                listaWypozyczen.zwrocKsiazke(ksiazkaOptional.get());
                System.out.println("Książka "+ksiazkaOptional.get().getTytulKsiazki()+" wypożyczona przez "+zalogowanyUzytkownik.getImie()+" "+zalogowanyUzytkownik.getNazwisko()+" została zwrócona przez "+zalogowanyUzytkownik.getImie()+" "+zalogowanyUzytkownik.getNazwisko()+".");
                warunekPetli = false;
            }
        } while (warunekPetli);
        menuGlowne();
    }

    private void sprawdzListeWypozyczonychKsiazekMenu(){
        Narzedzia.wait((double) 1);
        System.out.println("Lista książek wypożyczonych przez " + zalogowanyUzytkownik.getImie() + " " + zalogowanyUzytkownik.getNazwisko());
        if(listaWypozyczen.getListaWypozyczen()!=null) {
            if(!listaWypozyczen.getListaWypozyczen().isEmpty()) {
                if (listaWypozyczen.get(zalogowanyUzytkownik) == null) {
                    System.out.println("Nie masz żadnej, wypożyczonej książki");
                } else {
                    System.out.println(listaWypozyczen.get(zalogowanyUzytkownik));
                }
            }
        } else {
            System.out.println("Nie masz żadnej, wypożyczonej książki");
        }
        menuGlowne();
    }

    private void znajdzKsiazkiKtoraCieInteresujaMenu() {
        Narzedzia.wait((double) 1);
        System.out.println("\nJakiej książki szukasz? Wpisz nazwisko autora, fragment tytułu, gatunek książki, lub inną informację. Postaram się znaleźć wszystkie książki, które będą pasowały do Twojego opisu.");
        ListaKsiazek listaPasujacychKsiazek = new ListaKsiazek(listaKsiazek.znajdzPasujaceKsiazki(readLine()));
        System.out.println(listaPasujacychKsiazek);
        menuGlowne();
    }

    private String readLine() {
        String tekst = null;
        try {
            tekst = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tekst;
    }

    public void podmienBufferedReaderDoTestow(BufferedReader bufferedReader) {
        br = bufferedReader;
    }
}
