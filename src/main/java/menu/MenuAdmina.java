package menu;

import ksiazka.Gatunek;
import ksiazka.Ksiazka;
import ksiazka.ListaKsiazek;
import listaWypozyczen.ListaWypozyczen;
import narzedzia.Narzedzia;
import uzytkownik.ListaUzytkownikow;
import uzytkownik.PeselValidator;
import uzytkownik.Uzytkownik;

import java.io.*;
import java.util.Objects;
import java.util.Optional;

public class MenuAdmina {

    private Uzytkownik zalogowanyUzytkownik;
    private BufferedReader br;
    private ListaKsiazek listaKsiazek;
    private ListaWypozyczen listaWypozyczen;
    private ListaUzytkownikow listaUzytkownikow;

    public MenuAdmina(Uzytkownik zalogowanyUzytkownik, ListaKsiazek listaKsiazek, ListaWypozyczen listaWypozyczen, ListaUzytkownikow listaUzytkownikow) {
        this.zalogowanyUzytkownik = zalogowanyUzytkownik;
        this.listaKsiazek = listaKsiazek;
        this.listaUzytkownikow = listaUzytkownikow;
        br = new BufferedReader(new InputStreamReader(System.in));
        this.listaWypozyczen = Objects.requireNonNullElseGet(listaWypozyczen, () -> new ListaWypozyczen(zalogowanyUzytkownik, listaUzytkownikow, listaKsiazek));
    }

    public void uruchomMenu() {
        Narzedzia.wait((double) 1);
        System.out.println("\nCześć "+zalogowanyUzytkownik.getImie()+"!");
        menuGlowne();
    }

    private void menuGlowne() {
        Narzedzia.pressAnyKeyToContinue();
        System.out.println("\nWprowadź numer akcji, którą chcesz wykonać:");
        System.out.println("1. Dodaj książkę do biblioteki.");
        System.out.println("2. Usuń książkę z biblioteki.");
        System.out.println("3. Sprawdź listę wypożyczonych książek danego użytkownika.");
        System.out.println("4. Zapisz liste wypożyczeń do pliku.");
        System.out.println("5. Załaduj listę wypożyczeń z pliku.");
        System.out.println("6. Zapisz liste książek do pliku.");
        System.out.println("7. Załaduj listę książek z pliku.");
        System.out.println("8. Zapisz liste użytkowników do pliku.");
        System.out.println("9. Załaduj listę użytkowników z pliku.");
        System.out.println("10. Wyloguj się.");

        int wybranaOpcja = Narzedzia.sprawdzWprowadznaWartoscMenu(1,10);
        switch (wybranaOpcja){
            case 1:
                dodajKsiazkeDoBiblioteki();
                break;
            case 2:
                usunKsiazkeZBiblioteki();
                break;
            case 3:
                sprawdzListeWypozyczonychKsiazekDanegoUzytkownika();
                break;
            case 4:
                zapiszListeWypozyczenDoPliku();
                break;
            case 5:
                wczytajListeWypozyczenZPliku();
                break;
            case 6:
                zapiszListeKsiazekDoPliku();
                break;
            case 7:
                wczytajListeKsiazekZPliku();
                break;
            case 8:
                zapiszListeUzytkownikowDoPliku();
                break;
            case 9:
                wczytajListeUzytkownikowZPliku();
                break;
            case 10:
                break;
        }
    }

    private void dodajKsiazkeDoBiblioteki(){
        System.out.println("Podaj dane książki.");
        System.out.println("Tytuł książki: ");
        String tytulKsiazki = readLine();
        System.out.println("Rok wydania: ");
        int rokWydania = Narzedzia.sprawdzWprowadznaWartoscMenu(1,3000);
        System.out.println("ISBN: ");
        String isbn = readLine();
        System.out.println("Autor: ");
        String autor = readLine();
        System.out.println("Wydawca: ");
        String wydawca = readLine();
        System.out.println("Gatunek: ");
        System.out.println("Lista gatunków: "+Gatunek.SIFI+" "+Gatunek.ROMANS+" "+Gatunek.BASN+" "+Gatunek.HISTORYCZNA+" "+Gatunek.KRYMINAL+" "+Gatunek.POPULARNONAUKOWA);
        boolean warunekPetli;
        String gatunek;
        do {
            gatunek = readLine().toUpperCase();
            if (!(gatunek.equals(Gatunek.SIFI.name())
                    || gatunek.equals(Gatunek.ROMANS.name())
                    || gatunek.equals(Gatunek.BASN.name())
                    || gatunek.equals(Gatunek.HISTORYCZNA.name())
                    || gatunek.equals(Gatunek.KRYMINAL.name())
                    || gatunek.equals(Gatunek.POPULARNONAUKOWA.name()))){
                warunekPetli = true;
                System.out.println("Podałeś niepoprawną wartość. Spróbuj jeszcze raz.");
            } else {
                warunekPetli = false;
            }
        }while (warunekPetli);
        System.out.println("Ilość stron: ");
        int iloscStron = Narzedzia.sprawdzWprowadznaWartoscMenu(1,Integer.MAX_VALUE);
        System.out.println("Ile sztuk takich siążek chcesz dodać? Możesz dodać maksymalnie 100 sztuk na raz.");
        int iloscSztuk = Narzedzia.sprawdzWprowadznaWartoscMenu(1,100);


        for (int i = 0; i < iloscSztuk; i++) {
            Ksiazka nowaKsiazka = new Ksiazka(tytulKsiazki, rokWydania, isbn, autor, wydawca, Gatunek.valueOf(gatunek), iloscStron);
            listaKsiazek.dodajKsiazke(nowaKsiazka);
        }
        menuGlowne();
    }

    private void usunKsiazkeZBiblioteki(){
        System.out.println("Podaj numer biblioteczny książki, którą chcesz usunąć z biblioteki.");
        Optional<Ksiazka> szukanaKsiazka = listaKsiazek.znajdzKsiazke(readLine());
        szukanaKsiazka.ifPresent(ksiazka -> listaKsiazek.usunKsiazke(ksiazka));
        menuGlowne();
    }

    private void sprawdzListeWypozyczonychKsiazekDanegoUzytkownika(){
        System.out.println("Podaj PESEL lub numer biblioteczny użytkownika. Zaraz znajdę wszystkie książki, jakie on wypożyczył.");
        System.out.println("1.Wyszukaj po PESEL-u.\n 2.Wyszukaj po Numerze Karty Bibliotecznej.");
        int wybor = Narzedzia.sprawdzWprowadznaWartoscMenu(1,2);
        Optional<Uzytkownik> wyszukanyUzytkownik = Optional.empty();
        switch (wybor){
            case 1:
                boolean notValid;
                String pesel;
                do {
                    System.out.println("Podaj Pesel:");
                    pesel = readLine();
                    notValid = !PeselValidator.valid(pesel);
                } while (notValid);
                wyszukanyUzytkownik = listaUzytkownikow.znajdzUzytkownika(pesel, null);
                break;
            case 2:
                System.out.println("Podaj Numer Karty Biblotecznej(KB-00000):");
                String nrKarty = readLine();
                listaUzytkownikow.znajdzUzytkownika(null, nrKarty);
                wyszukanyUzytkownik = listaUzytkownikow.znajdzUzytkownika(null, nrKarty);
                break;
        }
        if (wyszukanyUzytkownik.isPresent()) {
            Narzedzia.wait((double) 1000);
            System.out.println(listaWypozyczen.get(wyszukanyUzytkownik.get()));
        } else {
            System.out.println("Nie znaleziono użytkownika.\n1.Spróbuj ponownie.\n2.Powrót do menu głównego.");
            wybor = Narzedzia.sprawdzWprowadznaWartoscMenu(1,2);
            switch (wybor){
                case 1:
                    sprawdzListeWypozyczonychKsiazekDanegoUzytkownika();
                    break;
                case 2:
                    break;
            }
        }
    }

    private void zapiszListeWypozyczenDoPliku(){
        System.out.println("Podaj nazwę pliku, do którego chcesz zapisać listę wypożyczeń np. ListaWypozyczen.txt");
        String nazwaPliku = readLine();
        boolean udaloSie = listaWypozyczen.zapisDoPliku(nazwaPliku);
        if (udaloSie){
            System.out.println("Plik "+nazwaPliku+" zapisany");
        }else {
            System.out.println("Zapis do pliku nie powiódł się.");
        }
        menuGlowne();
    }

    private void wczytajListeWypozyczenZPliku(){
        System.out.println("Podaj nazwę pliku, z którego chcesz odczytać listę wypożyczeń np. ListaWypozyczen.txt");
        String nazwaPliku = readLine();
        boolean udaloSie = listaWypozyczen.odczytZPliku(nazwaPliku);
        if (udaloSie){
            System.out.println("Plik "+nazwaPliku+" wczytany");
        }else {
            System.out.println("Odczyt z pliku nie powiódł się.");
        }
        menuGlowne();
    }

    private void zapiszListeKsiazekDoPliku(){
        System.out.println("Podaj nazwę pliku, do którego chcesz zapisać listę książe np. ListaKsiazek.txt");
        String nazwaPliku = readLine();
        boolean udaloSie = listaKsiazek.zapisDoPliku(nazwaPliku);
        if (udaloSie){
            System.out.println("Plik "+nazwaPliku+" zapisany");
        }else {
            System.out.println("Zapis do pliku nie powiódł się.");
        }
        menuGlowne();
    }

    private void wczytajListeKsiazekZPliku(){
        System.out.println("Podaj nazwę pliku, z którego chcesz odczytać listę książe np. ListaKsiazek.txt");
        String nazwaPliku = readLine();
        boolean udaloSie = listaKsiazek.odczytZPliku(nazwaPliku, listaUzytkownikow);
        if (udaloSie){
            System.out.println("Plik "+nazwaPliku+" wczytany");
        }else {
            System.out.println("Odczyt z pliku nie powiódł się.");
        }
        menuGlowne();
    }

    private void zapiszListeUzytkownikowDoPliku(){
        System.out.println("Podaj nazwę pliku, do którego chcesz zapisać listę użytkowników np. ListaUzytkownikow.txt");
        String nazwaPliku = readLine();
        boolean udaloSie = listaUzytkownikow.zapisDoPliku(nazwaPliku);
        if (udaloSie){
            System.out.println("Plik "+nazwaPliku+" zapisany");
        }else {
            System.out.println("Zapis do pliku nie powiódł się.");
        }
        menuGlowne();
    }

    private void wczytajListeUzytkownikowZPliku(){
        System.out.println("Podaj nazwę pliku, z którego chcesz odczytać listę użytkowników np. ListaUzytkownikow.txt");
        String nazwaPliku = readLine();
        boolean udaloSie = listaUzytkownikow.odczytZPliku(nazwaPliku);
        if (udaloSie){
            System.out.println("Plik "+nazwaPliku+" wczytany");
        }else {
            System.out.println("Odczyt z pliku nie powiódł się.");
        }
        menuGlowne();
    }


    private String readLine(){
        String tekst = null;
        try {
            tekst = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tekst;
    }

    public void podmienBufferedReaderDoTestow (BufferedReader bufferedReader){
        br = bufferedReader;
    }

}
