package uzytkownik;

import java.util.Optional;

public interface ListaUzytkownikowInterface {

    boolean dodajUzytkowniaka(Uzytkownik uzytkownik);
    boolean usunUzytkowniaka(Uzytkownik uzytkownik);
    Optional<Uzytkownik> znajdzUzytkownika(String pesel, String numerBibloteczny);
    boolean modyfikujUzytkownika(Uzytkownik uzytkownik);
    String toString();

}
