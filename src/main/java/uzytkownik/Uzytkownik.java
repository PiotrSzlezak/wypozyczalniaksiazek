package uzytkownik;

import java.io.Serializable;

public class Uzytkownik implements Serializable {

    private static final String hasloAdmina = "BardzoSilneHaslo";
    private static int licznikKarta = 11111;
    private String imie;
    private String nazwisko;
    private String pesel;
    private String numerBibloteczny;
    private String email;
    private PoziomUprawnien poziomUprawnien = PoziomUprawnien.KLIENT;

    public Uzytkownik(String imie, String nazwisko, String pesel, String email) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        if(PeselValidator.valid(pesel)){
            this.pesel=pesel;
        }else {
            System.out.println("Niepoprawny PESEL.");
        }
        this.numerBibloteczny = wygenerujNumberBibloteczny();
        this.email = email;
    }

    private String wygenerujNumberBibloteczny() {
        numerBibloteczny = "KB-" + licznikKarta;
        licznikKarta++;
        return numerBibloteczny;
    }

    public void setPesel(String pesel) {
        if (PeselValidator.valid(pesel)) {
            this.pesel = pesel;
        } else {
            System.out.println("Niepoprawny PESEL.");
        }
    }


    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getPesel() {
        return pesel;
    }

    public String getNumerBibloteczny() {
        return numerBibloteczny;
    }

    public String getEmail() {
        return email;
    }

    public PoziomUprawnien getPoziomUprawnien() {return poziomUprawnien;}

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static int getLicznikKarta() {
        return licznikKarta;
    }

    public static void setLicznikKarta(int licznikKarta) {
        Uzytkownik.licznikKarta = licznikKarta;
    }

    public boolean nadajUprawnieniaAdministratorskie(String haslo){
        boolean poprawneHaslo = false;
        if (haslo.equals(hasloAdmina)){
            poprawneHaslo = true;
            poziomUprawnien = PoziomUprawnien.ADMIN;
        }
        return poprawneHaslo;
    }
}

