package uzytkownik;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import narzedzia.Narzedzia;
import obslugaPlikow.ZapisIOdczytZPliku;
import wysylkaPowiadomien.WyslkaEmail;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;


public class ListaUzytkownikow implements ListaUzytkownikowInterface, ZapisIOdczytZPliku {
    private ArrayList<Uzytkownik> listaUzytkownikow = new ArrayList<>();
    private static int licznikKarta = 11111;


    @Override
    public boolean dodajUzytkowniaka(Uzytkownik uzytkownik) {
        long count = listaUzytkownikow.stream().filter(u -> u.getPesel().equals(uzytkownik.getPesel())).count();
        if (count == 0) {
            listaUzytkownikow.add(uzytkownik);
            System.out.println("Dodano użytkownika:" + uzytkownik.getImie() + " " + uzytkownik.getNazwisko()
                    + " PESEL: "+uzytkownik.getPesel()+" E-mail: "+uzytkownik.getEmail());
            WyslkaEmail wyslkaEmail = new WyslkaEmail();
            wyslkaEmail.wyslkaEmailAsync(uzytkownik.getEmail(),"Wypożyczalnia Książek - Nowy użytkownik"
                    ,"Witaj "+uzytkownik.getImie()+"," +
                            "\n\nWłaśnie zostało utworzone nowe konto w naszym serwisie." +
                            "\nNumer Twojej karty bibliotecznej: "+uzytkownik.getNumerBibloteczny()+"." +
                            "\nZapraszamy do korzystania naszej wypożyczalni."+"\n\nPozdrawiamy,\nZespół Wypożyczalnia Książek");
            return true;
        } else {
            System.out.println("Taki użytkownik istnieje!");
        }
        return false;
    }

    @Override
    public boolean usunUzytkowniaka(Uzytkownik uzytkownik) {
        if (listaUzytkownikow.contains(uzytkownik)) {
            listaUzytkownikow.remove(uzytkownik);
            System.out.println("Użytkownik usunięty: " + uzytkownik.getImie() + " " + uzytkownik.getNazwisko());
            return true;
        } else {
            System.out.println("Brak użytkownika!");
        }
        return false;
    }

    @Override
    public Optional<Uzytkownik> znajdzUzytkownika(String pesel, String numerBibloteczny) {
        Optional<Uzytkownik> uzytkownik = listaUzytkownikow.stream().filter(u -> u.getNumerBibloteczny()
                .equals(numerBibloteczny)).findFirst();
        if (!(pesel == null)) {
            uzytkownik = listaUzytkownikow.stream()
                    .filter(u -> u.getPesel().equals(pesel)).findFirst();
        }
        return uzytkownik;
    }

    @Override
    public boolean modyfikujUzytkownika(Uzytkownik uzytkownik) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Modyfikacja danych:\n1.Zmień imię.\n2.Zmień nazwisko.\n3.Zmień E-mail." +
                "\n4.Wróć do modyfikacji użytkownika.");
        int menu1 = Narzedzia.sprawdzWprowadznaWartoscMenu(1, 4);
        switch (menu1) {
            case 1:
                System.out.print("Zmieniasz imię " + uzytkownik.getImie() + " na: ");
                uzytkownik.setImie(sc.next());
                System.out.println("Imię zmodyfikowano.");
                modyfikujUzytkownika(uzytkownik);
                break;
            case 2:
                System.out.print("Zmieniasz nazwisko " + uzytkownik.getNazwisko() + " na: ");
                uzytkownik.setNazwisko(sc.next());
                System.out.println("Nazwisko zmodyfikowano.");
                modyfikujUzytkownika(uzytkownik);
                break;
            case 3:
                System.out.print("Zmieniasz E-mail " + uzytkownik.getEmail() + " na: ");
                uzytkownik.setEmail(sc.next());
                System.out.println("E-mail zmodyfikowano.");
                modyfikujUzytkownika(uzytkownik);
                break;
            case 4:
                return false;
        }
        return true;
    }

    @Override
    public String toString() {

        if (listaUzytkownikow == null) {
            return "Lista użytkowników jest pusta.";
        }

        String listauzytkownikowString = "";
        int iloscUzytkownikow = 0;

        for (Uzytkownik uzytkownik : listaUzytkownikow) {

            listauzytkownikowString = listauzytkownikowString +
                    String.format("%-15s", "Imię: " + uzytkownik.getImie())
                    + String.format("%-25s", "Nazwisko: " + uzytkownik.getNazwisko())
                    + String.format("%-20s", "PESEL: " + uzytkownik.getPesel())
                    + String.format("%-30s", "E-mail: "+ uzytkownik.getEmail())
                    + String.format("%-20s", "Nr Karty: " + uzytkownik.getNumerBibloteczny())
                    + String.format("%-20s", "Typ użytkownika: " + uzytkownik.getPoziomUprawnien().name()+"\n");
            iloscUzytkownikow++;
        }

        return "Ilość użytkowników: " + iloscUzytkownikow + "."+"\n" + listauzytkownikowString;
    }

    public ArrayList<Uzytkownik> getListaUzytkownikow() {
        return listaUzytkownikow;
    }

    @Override
    public boolean zapisDoPliku(String scierzkaDoPliku){
        boolean udaloSie;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        licznikKarta = Uzytkownik.getLicznikKarta();
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(scierzkaDoPliku));
            bufferedWriter.write(gson.toJson(this));
            bufferedWriter.close();
            udaloSie = true;
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Plik z listą użytkowników nie istnieje, lub nie można go otworzyć");
            udaloSie = false;
        }
        return udaloSie;
    }

    @Override
    public boolean odczytZPliku(String scierzkaDoPliku) {
        boolean udaloSie;
        Gson gson = new Gson();
        BufferedReader bufferedReader;

        try {
            bufferedReader = new BufferedReader(new FileReader(scierzkaDoPliku));
            ListaUzytkownikow listaUzytkownikowTemp = gson.fromJson(bufferedReader, ListaUzytkownikow.class);
            listaUzytkownikow = listaUzytkownikowTemp.getListaUzytkownikow();
            udaloSie = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Plik z listą użytkowników nie istnieje lub nie można go otworzyć");
            udaloSie = false;
        }
        Uzytkownik.setLicznikKarta(licznikKarta);
        return udaloSie;
    }
}

