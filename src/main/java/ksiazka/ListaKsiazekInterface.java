package ksiazka;

import java.util.ArrayList;
import java.util.Optional;

public interface
ListaKsiazekInterface {

    boolean dodajKsiazke (Ksiazka ksiazka);
    boolean usunKsiazke (Ksiazka ksiazka);
    Optional<Ksiazka> znajdzKsiazke (String nrBibliotecznyKsiazki);
    ArrayList<Ksiazka> znajdzPasujaceKsiazki(String szukanaFraza);
    ArrayList<Ksiazka> listaUnikatowychKsiazek();
    String toString ();

}
