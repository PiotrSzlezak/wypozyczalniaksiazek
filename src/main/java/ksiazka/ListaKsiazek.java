package ksiazka;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import obslugaPlikow.ZapisIOdczytZPliku;
import uzytkownik.ListaUzytkownikow;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ListaKsiazek implements ListaKsiazekInterface, ZapisIOdczytZPliku {

    private static String najnowszyNumerBibliotecznyKsiazki;

    private ArrayList<Ksiazka> listaKsiazek;
    transient private ArrayList<Ksiazka> listaUnikatowychKsiazek;

    public ListaKsiazek() {
        listaKsiazek = new ArrayList<>();
        listaUnikatowychKsiazek = new ArrayList<>();
    }

    public ListaKsiazek(ArrayList<Ksiazka> listaKsiazek) {
        this.listaKsiazek = listaKsiazek;
        listaUnikatowychKsiazek();
    }

    @Override
    public boolean dodajKsiazke(Ksiazka ksiazka) {

        if(listaKsiazek.contains(ksiazka)){
            System.out.println("Książka o podanym numerze biblioteczny już istnieje w naszej bazie danych.");
            return false;
        }
        listaKsiazek.add(ksiazka);
//        System.out.println("Książka została dodana do naszej bazy danych.");
        return true;
    }

    @Override
    public boolean usunKsiazke(Ksiazka ksiazka) {
        if(!listaKsiazek.contains(ksiazka)) {
            System.out.println("Książka o podanym numerze biblioteczny nie została odnaleziona w naszej bazie danych.");
            return false;
        }
        listaKsiazek.remove(ksiazka);
//        System.out.println("Książka została usunięta z naszej bazy danych.");
        return true;
    }

    @Override
    public Optional<Ksiazka> znajdzKsiazke(String nrBibliotecznyKsiazki) {

        Optional<Ksiazka> ksiazkaOptional = listaKsiazek.stream()
                .filter(book -> book.getNrBibliotecznyKsiazki().equals(nrBibliotecznyKsiazki))
                .findFirst();

        return ksiazkaOptional;
    }

    @Override
    public ArrayList<Ksiazka> znajdzPasujaceKsiazki(String szukanaFraza) {
        listaUnikatowychKsiazek();

        ArrayList<Ksiazka> listaPasujacychKsiazek = (ArrayList<Ksiazka>)listaUnikatowychKsiazek.stream()
                .filter(k -> czyFrazaPasujeDoKsiazki(k, szukanaFraza))
                .collect(Collectors.toList());

        return listaPasujacychKsiazek;
    }

    @Override
    public ArrayList<Ksiazka> listaUnikatowychKsiazek() {
        listaUnikatowychKsiazek = new ArrayList<>();

        for (Ksiazka ksiazka: listaKsiazek) {
            String isbn = ksiazka.getIsbn();
            if (listaUnikatowychKsiazek.isEmpty()){
                listaUnikatowychKsiazek.add(ksiazka);
            }
            boolean czyKsiazkaJestUnikatowa = true;
            for (Ksiazka unikatowaKsiazka: listaUnikatowychKsiazek) {

                if (isbn.equals(unikatowaKsiazka.getIsbn())) {
                    czyKsiazkaJestUnikatowa = false;
                    break;
                }
            }
            if (czyKsiazkaJestUnikatowa) {
                listaUnikatowychKsiazek.add(ksiazka);
            }
        }

        return null;
    }

    private boolean czyFrazaPasujeDoKsiazki(Ksiazka ksiazka, String szukanaFraza){
        szukanaFraza = szukanaFraza.toLowerCase();
        Pattern pattern = Pattern.compile(szukanaFraza);
        Matcher tytulKsiazki = pattern.matcher(ksiazka.getTytulKsiazki().toLowerCase());
        Matcher autor = pattern.matcher(ksiazka.getAutor().toLowerCase());
        Matcher wydawca = pattern.matcher(ksiazka.getWydawca().toLowerCase());
        Matcher gatunek = pattern.matcher(String.valueOf(ksiazka.getGatunek()).toLowerCase());
        Matcher isbn = pattern.matcher(ksiazka.getIsbn().toLowerCase());
        Matcher nrBibliotecznyKsiazki = pattern.matcher(ksiazka.getNrBibliotecznyKsiazki().toLowerCase().toLowerCase());
        Matcher rokWydania = pattern.matcher(String.valueOf(ksiazka.getRokWydania()));
        Matcher iloscStron = pattern.matcher(String.valueOf(ksiazka.getIloscStron()));

        boolean test =
                tytulKsiazki.find()
                || autor.find()
                || wydawca.find()
                || gatunek.find()
                || isbn.matches()
                || nrBibliotecznyKsiazki.matches()
                || rokWydania.matches()
                || iloscStron.matches();

        return test;
    }

    @Override
    public String toString(){

        if (listaKsiazek==null){
            return "Lista książek jest pusta.";
        }

//        StringBuilder listaKsiazekSB = new StringBuilder();
        String listaKsiazekString = "";
        int iloscKsiazek = 0;

        for (Ksiazka ksiazka: listaKsiazek) {

            String takNie;
            if(ksiazka.getCzyKsiazkaJestWypozyczona())
            { takNie = "Tak";}
            else { takNie = "Nie"; }

            listaKsiazekString = listaKsiazekString +
                        String.format("%-50s","Tytuł książki: "+ksiazka.getTytulKsiazki())
                        + String.format("%-40s","Autor: "+ksiazka.getAutor())
                        + String.format("%-40s","Wydana przez: "+ksiazka.getWydawca())
                        + "\n"
                        + String.format("%-50s","Rok wydania: "+ksiazka.getRokWydania())
                        + String.format("%-40s","Gatunek: "+ksiazka.getGatunek())
                        + String.format("%-40s","Ilość stron: "+ksiazka.getIloscStron())
                        + "\n"
                        + String.format("%-50s","Czy książka jest wypożyczona?: "+takNie)
                        + String.format("%-40s","ISBN: "+ksiazka.getIsbn())
                        + String.format("%-40s","Nr biblioteczny: "+ksiazka.getNrBibliotecznyKsiazki())
                        + "\n"
                        + String.format("%-50s","Data wypożyczenia: "+ksiazka.getAktualnaData())
                        + String.format("%-40s","Planowana data zwrotu: "+ksiazka.getDataZwrotu())
                        +"\n\n";


//            listaKsiazekSB.append(listaKsiazekString)
//                    .append(String.format("%-50s","Tytuł książki: "+ksiazka.getTytulKsiazki()))
//                    .append(String.format("%-40s","Autor: "+ksiazka.getAutor()))
//                    .append(String.format("%-40s","Wydana przez: "+ksiazka.getWydawca()))
//                    .append("\n")
//                    .append(String.format("%-50s","Rok wydania: "+ksiazka.getRokWydania()))
//                    .append(String.format("%-40s","Gatunek: "+ksiazka.getGatunek()))
//                    .append(String.format("%-40s","Ilość stron: "+ksiazka.getIloscStron()))
//                    .append("\n")
//                    .append(String.format("%-50s","Czy książka jest wypożyczona?: "+takNie))
//                    .append(String.format("%-40s","ISBN: "+ksiazka.getIsbn()))
//                    .append(String.format("%-40s","Nr biblioteczny: "+ksiazka.getNrBibliotecznyKsiazki()))
//                    .append("\n\n");
//
//            listaKsiazekString = listaKsiazekSB.toString();

            iloscKsiazek++;
        }

//        return "W bibliotece jest "+iloscKsiazek+" książek.\n\n"+listaKsiazekSB.toString();
        return "Ilość książek na liście: "+iloscKsiazek+"\n\n"+listaKsiazekString;
    }

    @Override
    public boolean zapisDoPliku(String scierzkaDoPliku){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        boolean udaloSie;
        najnowszyNumerBibliotecznyKsiazki = Ksiazka.getNajnowszyNumerBibliotecznyKsiazki();

        try {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(scierzkaDoPliku));
        bufferedWriter.write(gson.toJson(this));
        bufferedWriter.close();
        udaloSie = true;
        } catch(IOException e){
            e.printStackTrace();
            System.out.println("Błąd podczas próby zapisy listy książek do pliku");
            udaloSie = false;
        }
        return udaloSie;
    }

    @Override
    public boolean odczytZPliku(String scierzkaDoPliku) {
        return false;
    }

    public boolean odczytZPliku(String scierzkaDoPliku, ListaUzytkownikow listaUzytkownikow) {
        boolean udaloSie;
        Gson gson = new Gson();
        BufferedReader bufferedReader;

        try {
            bufferedReader = new BufferedReader(new FileReader(scierzkaDoPliku));
            ListaKsiazek listaKsiazekTemp = gson.fromJson(bufferedReader, ListaKsiazek.class);
            listaKsiazek = listaKsiazekTemp.getListaKsiazek();
            for (Ksiazka book:listaKsiazek) {
                book.przypiszUzytkownikowNaBazieIchNumerowBibliotecznych(listaUzytkownikow);
            }
            listaUnikatowychKsiazek();
            udaloSie = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Plik z listą książek nie istnieje, lub nie można go otworzyć");
            udaloSie = false;
        }

        Ksiazka.setNajnowszyNumerBibliotecznyKsiazki(najnowszyNumerBibliotecznyKsiazki);
        return udaloSie;
    }

    public ArrayList<Ksiazka> getListaKsiazek() {
        return listaKsiazek;
    }


}
