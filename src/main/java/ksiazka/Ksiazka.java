package ksiazka;

import uzytkownik.ListaUzytkownikow;
import uzytkownik.Uzytkownik;

import java.time.LocalDate;
import java.util.Optional;

public class Ksiazka {

    private static String najnowszyNumerBibliotecznyKsiazki;

    private String tytulKsiazki;
    private int rokWydania;
    private String isbn;
    private String autor;
    private String wydawca;
    private Gatunek gatunek;
    private int iloscStron;
    private String nrBibliotecznyKsiazki;
    private boolean czyKsiazkaJestWypozyczona = false;
    transient private Uzytkownik uzytkownikObecniePosiadajacyTaKsiazke;
    private LocalDate dataWypozyczenia = null;
    private LocalDate dataZwrotu = null;
    private String numerUzytkownikObecniePosiadajacyTaKsiazke;


    public Ksiazka(String tytulKsiazki, int rokWydania, String isbn, String autor, String wydawca, Gatunek gatunek, int iloscStron) {
        this.tytulKsiazki = tytulKsiazki;
        this.rokWydania = rokWydania;
        this.isbn = isbn;
        this.autor = autor;
        this.wydawca = wydawca;
        this.gatunek = gatunek;
        this.iloscStron = iloscStron;
        nrBibliotecznyKsiazki = wygenerujNumerBibliotecznyKsiazki();
    }

    private String wygenerujNumerBibliotecznyKsiazki() {
        if (najnowszyNumerBibliotecznyKsiazki == null) {
            najnowszyNumerBibliotecznyKsiazki = "ABC-1111";
            return najnowszyNumerBibliotecznyKsiazki;
        }

        int numer = Integer.parseInt(najnowszyNumerBibliotecznyKsiazki.substring(4)) + 1;
        najnowszyNumerBibliotecznyKsiazki = najnowszyNumerBibliotecznyKsiazki.substring(0, 4) + numer;
        return najnowszyNumerBibliotecznyKsiazki;
    }

    public void wypozyczKsiazke(Uzytkownik uzytkownik) {
        czyKsiazkaJestWypozyczona = true;
        uzytkownikObecniePosiadajacyTaKsiazke = uzytkownik;
        numerUzytkownikObecniePosiadajacyTaKsiazke = uzytkownik.getNumerBibloteczny();
        dataWypozyczenia = LocalDate.now();
        dataZwrotu = dataWypozyczenia.plusDays(21);
    }

    public void zwrocKsiazke() {
        czyKsiazkaJestWypozyczona = false;
        uzytkownikObecniePosiadajacyTaKsiazke = null;
        dataWypozyczenia = null;
        dataZwrotu = null;
    }

    public boolean przypiszUzytkownikowNaBazieIchNumerowBibliotecznych(ListaUzytkownikow listaUzytkownikow){
        Optional<Uzytkownik> uzytkownik = listaUzytkownikow.znajdzUzytkownika(null, numerUzytkownikObecniePosiadajacyTaKsiazke);
        boolean udaloSie = false;
        if (uzytkownik.isPresent()){
            uzytkownikObecniePosiadajacyTaKsiazke = uzytkownik.get();
            udaloSie = true;
        } else {
            udaloSie = false;
        }
        return udaloSie;
    }

    public String getTytulKsiazki() {
        return tytulKsiazki;
    }

    public int getRokWydania() {
        return rokWydania;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getAutor() {
        return autor;
    }

    public String getWydawca() {
        return wydawca;
    }

    public Gatunek getGatunek() {
        return gatunek;
    }

    public int getIloscStron() {
        return iloscStron;
    }

    public String getNrBibliotecznyKsiazki() {
        return nrBibliotecznyKsiazki;
    }

    public boolean getCzyKsiazkaJestWypozyczona() {
        return czyKsiazkaJestWypozyczona;
    }

    public Uzytkownik getUzytkownikObecniePosiadajacyTaKsiazke() {
        return uzytkownikObecniePosiadajacyTaKsiazke;
    }

    public static String getNajnowszyNumerBibliotecznyKsiazki() {
        return najnowszyNumerBibliotecznyKsiazki;
    }

    public static void setNajnowszyNumerBibliotecznyKsiazki(String najnowszyNumerBibliotecznyKsiazki) {
        Ksiazka.najnowszyNumerBibliotecznyKsiazki = najnowszyNumerBibliotecznyKsiazki;
    }
    public LocalDate getAktualnaData() {
        return dataWypozyczenia;
    }

    public LocalDate getDataZwrotu() {
        return dataZwrotu;
    }

}
