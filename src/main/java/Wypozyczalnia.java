import ksiazka.Gatunek;
import ksiazka.Ksiazka;
import uzytkownik.Uzytkownik;
import menu.MenuAdmina;
import ksiazka.ListaKsiazek;
import listaWypozyczen.ListaWypozyczen;
import menu.Logowanie;
import uzytkownik.ListaUzytkownikow;


public class Wypozyczalnia {

    private static ListaUzytkownikow listaUzytkownikow = new ListaUzytkownikow();
    private static ListaKsiazek listaKsiazek = new ListaKsiazek();
    private static ListaWypozyczen listaWypozyczen = new ListaWypozyczen();

    public static void main(String[] arg1s) {

        //Ręczna deklaracja wszystkich obiektów
//        Uzytkownik uzytkownikPiotr = new Uzytkownik("Piotr","Szlęzak","98120266628","piotr@szlezak.pl");
//        Uzytkownik uzytkownikRobert = new Uzytkownik("Robert","Test","99063037287","robus.ll@gmail.com");
//        listaUzytkownikow.dodajUzytkowniaka(uzytkownikRobert);
//        listaUzytkownikow.dodajUzytkowniaka(uzytkownikPiotr);
//        Ksiazka ksiazka1 = new Ksiazka("Książka 1", 1, "ISBN 1", "Jan Kowalski", "Wydawca 1", Gatunek.SIFI, 11);
//        Ksiazka ksiazka2 = new Ksiazka("Książka 2", 2, "ISBN 2", "Jan Nowak", "Wydawca 2", Gatunek.SIFI, 22);
//        Ksiazka ksiazka3 = new Ksiazka("Książka 3", 3, "ISBN 3", "Artur Nowakowski", "Wydawca 3", Gatunek.ROMANS, 33);
//        listaKsiazek.dodajKsiazke(ksiazka1);
//        listaKsiazek.dodajKsiazke(ksiazka2);
//        listaKsiazek.dodajKsiazke(ksiazka3);

        wczytajPlikiPrzyUruchomieniuProgramu();
//        System.out.println(listaUzytkownikow+"\n\n");
//        System.out.println(listaKsiazek+"\n\n");
//        System.out.println(listaWypozyczen+"\n\n");
//        listaUzytkownikow = listaUzytkownikow;
//        listaKsiazek = listaKsiazek;
//        listaWypozyczen = listaWypozyczen;

        Logowanie logowanie = new Logowanie(listaKsiazek, listaUzytkownikow, listaWypozyczen);
        logowanie.wyszukajUzytkownika();

        zapiszWszystkoPrzyZamknieciuProgramu();

        //Przejście bezpośrednio do menu admina
//        MenuAdmina menuAdmina = new MenuAdmina(listaUzytkownikow.znajdzUzytkownika("98120266628", null).get(), listaKsiazek, listaWypozyczen, listaUzytkownikow);
//        menuAdmina.uruchomMenu();
    }

    private static void zapiszWszystkoPrzyZamknieciuProgramu(){
        listaUzytkownikow.zapisDoPliku("ListaUzytkownikowDefault.txt");
        listaKsiazek.zapisDoPliku("ListaKsiazekDefault.txt");
        listaWypozyczen.zapisDoPliku("ListaWypozyczenDefault.txt");
        System.out.println("Wszystkie pliki zostały zapisane.");
    }

    private static void wczytajPlikiPrzyUruchomieniuProgramu(){
        listaUzytkownikow.odczytZPliku("ListaUzytkownikowDefault.txt");
        listaKsiazek.odczytZPliku("ListaKsiazekDefault.txt", listaUzytkownikow);
        listaWypozyczen.odczytZPliku("ListaWypozyczenDefault.txt", listaKsiazek, listaUzytkownikow);
        System.out.println("Wszystkie pliki zostały załadowane do programu.");
    }

}
