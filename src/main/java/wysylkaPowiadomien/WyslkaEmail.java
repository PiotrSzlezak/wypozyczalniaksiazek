package wysylkaPowiadomien;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class WyslkaEmail {

    final String username = "ksiazekwypozyczalnia@gmail.com";
    final String password = "BardzoSilneHaslo";

    public void wyslkaEmailAsync(String email, String tytulEmail, String wiadomoscEmail) {
        //System.out.println("before");
        new Thread(new Runnable() {
            @Override
            public void run() {
                //System.out.println("start");
                wyslkaEmail(email, tytulEmail, wiadomoscEmail);
                //System.out.println("stop");
            }
        }).start();
       // System.out.println("after");
    }

    public void wyslkaEmail(String email, String tytulEmail, String wiadomoscEmail) {


        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ksiazekwypozyczalnia@gmail.com"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(email)
            );
            message.setSubject(tytulEmail);
            message.setText(wiadomoscEmail);

            Transport.send(message);

            System.out.println("Wiadomość została wysłana na adres e-mail: "+email);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

