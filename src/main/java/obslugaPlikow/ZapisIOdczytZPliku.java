package obslugaPlikow;

import java.io.IOException;

public interface ZapisIOdczytZPliku {

    boolean zapisDoPliku(String scierzkaDoPliku) throws IOException;

    boolean odczytZPliku(String scierzkaDoPliku);

}
