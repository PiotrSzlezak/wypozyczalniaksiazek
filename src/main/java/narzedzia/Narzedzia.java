package narzedzia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

public class Narzedzia {

    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static int sprawdzWprowadznaWartoscMenu(int min, int max) {

        boolean warunekPetli;
        String wprowadzonaWartosc = null;

        do {
            try {
                wprowadzonaWartosc = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Optional<Integer> wprowadzonaWartoscOptional = sprawdzCzyWprowadzonaWartoscJestIntegeremZPodanegoZakresu(wprowadzonaWartosc, min, max);
            if (wprowadzonaWartoscOptional.isEmpty()) {
                System.out.println("Wprowadzona wartość była niepoprawna. Spróbuj jeszcze raz.");
                warunekPetli = true;
            } else {
                warunekPetli = false;
            }
        } while (warunekPetli);

        return Integer.parseInt(wprowadzonaWartosc);
    }

    private static Optional<Integer> sprawdzCzyWprowadzonaWartoscJestIntegeremZPodanegoZakresu(String wprowadzonaWartosc, int min, int max) {
        if (wprowadzonaWartosc.matches("\\d{1,9}")) {
            int wprowadzonaWartoscInt = Integer.parseInt(wprowadzonaWartosc);
            if (wprowadzonaWartoscInt>=min && wprowadzonaWartoscInt<=max) {
                return Optional.of(Integer.valueOf(wprowadzonaWartosc));
            }
        }
        return Optional.empty();
    }

    public static void podmienBufferedReaderDoTestow(BufferedReader bufferedReader) {
        br = bufferedReader;
    }

    public static void wait(double seconds) {
        try {
            System.out.print(".");
            Thread.sleep((int) (seconds * 1000 / 3));
            System.out.print(".");
            Thread.sleep((int) (seconds * 1000 / 3));
            System.out.print(".\n");
            Thread.sleep((int) (seconds * 1000 / 3));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void pressAnyKeyToContinue() {
        System.out.println("Wciśnij Enter any kontynuować.");
        try {
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
