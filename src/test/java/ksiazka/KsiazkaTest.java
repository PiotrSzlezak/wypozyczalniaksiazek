package ksiazka;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KsiazkaTest {

    @Test
    void testGenerowanieNumeruKsiazki() {
        // given
        Ksiazka ksiazka1 = new Ksiazka(null, 0, null,null,null,null,0);
        Ksiazka ksiazka2 = new Ksiazka(null, 0, null,null,null,null,0);

        // when
        String numerKsiazki1 = ksiazka1.getNrBibliotecznyKsiazki();
        String numerKsiazki2 = ksiazka2.getNrBibliotecznyKsiazki();

        // then
        Assertions.assertEquals("ABC-1111",numerKsiazki1);
        Assertions.assertEquals("ABC-1112",numerKsiazki2);
    }

}
