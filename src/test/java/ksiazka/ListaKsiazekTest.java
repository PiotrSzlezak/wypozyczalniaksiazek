package ksiazka;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import uzytkownik.ListaUzytkownikow;
import uzytkownik.Uzytkownik;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;

public class ListaKsiazekTest {
    private static ListaKsiazek listaKsiazek;
    private static Ksiazka ksiazka1 = new Ksiazka("Książka 1", 1, "ISBN 1", "Jan Kowalski", "Wydawca 1", Gatunek.SIFI, 11);
    private static Ksiazka ksiazka2 = new Ksiazka("Książka 2", 2, "ISBN 2", "Jan Nowak", "Wydawca 2", Gatunek.SIFI, 22);
    private static Ksiazka ksiazka3 = new Ksiazka("Książka 3", 3, "ISBN 3", "Artur Nowakowski", "Wydawca 3", Gatunek.ROMANS, 33);
    private static Uzytkownik uzytkownik1 = new Uzytkownik("Jan", "Kowalski", "71031291455","robus.ll@gmail.com");
    private static ListaUzytkownikow listaUzytkownikow;

    @BeforeEach
    void init (){
        listaKsiazek = new ListaKsiazek();
        listaUzytkownikow = new ListaUzytkownikow();
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik1);
    }

    @Test
    void testDodajPierwszaKsiazka(){
        // given


        // when
        listaKsiazek.dodajKsiazke(ksiazka1);

        // then
        Assert.assertTrue(listaKsiazek.getListaKsiazek().contains(ksiazka1));
        Assert.assertEquals(ksiazka1, listaKsiazek.getListaKsiazek().get(0));
        Assert.assertEquals(1,listaKsiazek.getListaKsiazek().size());
    }

    @Test
    void testDodajTaSamaKsiazke2Razy(){
        // given

        // when
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka1);

        // then
        Assert.assertTrue(listaKsiazek.getListaKsiazek().contains(ksiazka1));
        Assert.assertEquals(ksiazka1, listaKsiazek.getListaKsiazek().get(0));
        Assert.assertEquals(1,listaKsiazek.getListaKsiazek().size());
    }

    @Test
    void testDodaj3Ksiazki(){
        // given


        // when
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka2);
        listaKsiazek.dodajKsiazke(ksiazka3);

        // then
        Assert.assertTrue(listaKsiazek.getListaKsiazek().contains(ksiazka1));
        Assert.assertTrue(listaKsiazek.getListaKsiazek().contains(ksiazka2));
        Assert.assertTrue(listaKsiazek.getListaKsiazek().contains(ksiazka3));
        Assert.assertEquals(ksiazka1, listaKsiazek.getListaKsiazek().get(0));
        Assert.assertEquals(ksiazka2, listaKsiazek.getListaKsiazek().get(1));
        Assert.assertEquals(ksiazka3, listaKsiazek.getListaKsiazek().get(2));
        Assert.assertEquals(3,listaKsiazek.getListaKsiazek().size());
    }

    @Test
    void testUsunKsiazke(){
        //given
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka2);

        //when
        boolean usunKsiazke = listaKsiazek.usunKsiazke(ksiazka1);

        // then
        Assert.assertFalse(listaKsiazek.getListaKsiazek().contains(ksiazka1));
        Assert.assertTrue(listaKsiazek.getListaKsiazek().contains(ksiazka2));
        Assert.assertTrue(usunKsiazke);
        Assert.assertEquals(1,listaKsiazek.getListaKsiazek().size());
    }

    @Test
    void testUsunKsiazkeFail(){
        //given
        listaKsiazek.dodajKsiazke(ksiazka1);

        //when
        boolean usunKsiazke = listaKsiazek.usunKsiazke(ksiazka2);

        //then
        Assert.assertTrue(listaKsiazek.getListaKsiazek().contains(ksiazka1));
        Assert.assertFalse(usunKsiazke);
        Assert.assertEquals(1,listaKsiazek.getListaKsiazek().size());
    }

    @Test
    void testZnajdzKsiazke(){
        //given
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka2);

        //when
        Optional<Ksiazka> testKsiazka1 = listaKsiazek.znajdzKsiazke(ksiazka1.getNrBibliotecznyKsiazki());
        Optional<Ksiazka> testKsiazka2 = listaKsiazek.znajdzKsiazke(ksiazka2.getNrBibliotecznyKsiazki());

        //then
        Assert.assertEquals(ksiazka1,testKsiazka1.get());
        Assert.assertEquals(ksiazka2,testKsiazka2.get());
    }

    @Test
    void testZnajdzKsiazkeFail(){
        //given
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka2);

        //when
        Optional<Ksiazka> testKsiazka3 = listaKsiazek.znajdzKsiazke(ksiazka3.getNrBibliotecznyKsiazki());

        //then
        Assert.assertFalse(testKsiazka3.isPresent());
    }

    @Test
    void testCzyKsiazkaJestWypozyczona(){
        //given
        listaKsiazek.dodajKsiazke(ksiazka1);
    }

    @Test
    void testZnajdzPasujaceKsiazkiPoTekscie(){
        //given
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka2);
        listaKsiazek.dodajKsiazke(ksiazka3);

        //when
        ArrayList<Ksiazka> znalezionaListaKsiazek1 = listaKsiazek.znajdzPasujaceKsiazki("SIFI");
        ArrayList<Ksiazka> znalezionaListaKsiazek2 = listaKsiazek.znajdzPasujaceKsiazki("2");
        ArrayList<Ksiazka> znalezionaListaKsiazek3 = listaKsiazek.znajdzPasujaceKsiazki("ISBN");
        ArrayList<Ksiazka> znalezionaListaKsiazek4 = listaKsiazek.znajdzPasujaceKsiazki("Nowak");

        //then
        Assert.assertTrue(znalezionaListaKsiazek1.contains(ksiazka1));
        Assert.assertTrue(znalezionaListaKsiazek1.contains(ksiazka2));
        Assert.assertFalse(znalezionaListaKsiazek1.contains(ksiazka3));
        Assert.assertEquals(2, znalezionaListaKsiazek1.size());

        Assert.assertFalse(znalezionaListaKsiazek2.contains(ksiazka1));
        Assert.assertTrue(znalezionaListaKsiazek2.contains(ksiazka2));
        Assert.assertFalse(znalezionaListaKsiazek2.contains(ksiazka3));
        Assert.assertEquals(1, znalezionaListaKsiazek2.size());

        Assert.assertFalse(znalezionaListaKsiazek3.contains(ksiazka1));
        Assert.assertFalse(znalezionaListaKsiazek3.contains(ksiazka2));
        Assert.assertFalse(znalezionaListaKsiazek3.contains(ksiazka3));
        Assert.assertEquals(0, znalezionaListaKsiazek3.size());

        Assert.assertFalse(znalezionaListaKsiazek4.contains(ksiazka1));
        Assert.assertTrue(znalezionaListaKsiazek4.contains(ksiazka2));
        Assert.assertTrue(znalezionaListaKsiazek4.contains(ksiazka3));
        Assert.assertEquals(2, znalezionaListaKsiazek4.size());
    }

    @Test
    void shouldCreateListaKsiazeTextFile() throws IOException {
        //given
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka2);
        listaKsiazek.dodajKsiazke(ksiazka3);
        String stringPath = "Lista_ksiazek.txt";
        Path path = Paths.get(stringPath);
        Files.deleteIfExists(path);
        ksiazka1.wypozyczKsiazke(uzytkownik1);
        System.out.println(listaKsiazek);

        //when
        listaKsiazek.zapisDoPliku(stringPath);

        //then
        Assert.assertTrue(Files.exists(path));
    }

    @Test
    void shouldLoadListaKsiazekFromFile(){
        //given
        String stringPath = "ListaKsiazekDefault.txt";

        //when
        listaKsiazek.odczytZPliku(stringPath, listaUzytkownikow);

        //then
        Assert.assertEquals(3,listaKsiazek.getListaKsiazek().size());
    }

}