package uzytkownik;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class ListaUzytkownikowTest {

    private static ListaUzytkownikow listaUzytkownikow = new ListaUzytkownikow();
    private static Uzytkownik uzytkownik1 = new Uzytkownik("Mirosław", "Kowalik", "98120266628","test@onet.pl");
    private static Uzytkownik uzytkownik2 = new Uzytkownik("Jan", "Kowalski", "71031291455","test@onet.pl");
    private static Uzytkownik uzytkownik3 = new Uzytkownik("Robert", "Nowak", "48101676263","test@wp.pl");
    private static Uzytkownik uzytkownik4 = new Uzytkownik("Norbert", "Pietras", "04212172331","test@o2.pl");

    @BeforeEach
    void init (){
        listaUzytkownikow = new ListaUzytkownikow();
    }

    @Test
    public void shouldAddUzytkownik() {
        //given


        //when
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik2);

        //then
        Assert.assertFalse(listaUzytkownikow.getListaUzytkownikow().isEmpty());
        Assert.assertEquals(uzytkownik2, listaUzytkownikow.getListaUzytkownikow().get(0));

    }

    @Test
    public void souldRemoveUzytkownik() {
        //given


        //when
        listaUzytkownikow.usunUzytkowniaka(uzytkownik3);

        //then
        Assert.assertTrue(listaUzytkownikow.getListaUzytkownikow().isEmpty());
    }

    @Test
    public void souldFindUzytkownikPESEL() {
        //given


        listaUzytkownikow.dodajUzytkowniaka(uzytkownik4);
        //when
        Optional<Uzytkownik> uzytkownikOptional = listaUzytkownikow.znajdzUzytkownika("04212172331", null);


        //then

        Assert.assertEquals(uzytkownik4, uzytkownikOptional.get());
    }
    @Test
    public void souldFindUzytkownikNumerBiblotecz() {
        //given
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik1);
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik2);
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik4);
        //when
        Optional<Uzytkownik> uzytkownikOptional1 = listaUzytkownikow.znajdzUzytkownika(null, "KB-11114");


        //then

        Assert.assertEquals(uzytkownik4, uzytkownikOptional1.get());
    }
    @Test
    public void souldPirntUzytkownicy(){
        //given

        listaUzytkownikow.dodajUzytkowniaka(uzytkownik2);
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik3);
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik4);
        //when


        //then
        System.out.println(listaUzytkownikow.toString());
    }

    @Test
    public void souldCreateFile() throws IOException {
        //given
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik1);
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik2);
        listaUzytkownikow.dodajUzytkowniaka(uzytkownik3);

        String stringPath = "Lista_uzytkownikow.txt";
        Path path = Paths.get(stringPath);
        Files.deleteIfExists(path);

        //when
        listaUzytkownikow.zapisDoPliku(stringPath);

        //then
        Assert.assertTrue(Files.exists(path));

    }
    @Test
    public void shouldLoadListaUzytkownikowFromFile(){
        //given
        String stringPath = "Lista_uzytkownikow.txt";

        //when
        listaUzytkownikow.odczytZPliku(stringPath);

        //then
        Assert.assertEquals(4,listaUzytkownikow.getListaUzytkownikow().size());
    }
}