package uzytkownik;

import org.junit.Assert;
import org.junit.Test;

public class UzytkownikTest {

    Uzytkownik uzytkownik = new Uzytkownik("Mirosław", "Kowalik", "98120266628","test@onet.pl");
    Uzytkownik uzytkownik1 = new Uzytkownik("Jan", "Kowalski", "71031291455","test@onet.pl");
    Uzytkownik uzytkownik2 = new Uzytkownik("Robert", "Nowak", "48101676263","test@wp.pl");
    Uzytkownik uzytkownik3 = new Uzytkownik("Norbert", "Pietras", "04212172331","test@o2.pl");


    @Test
            public void shouldCreateKlient() {
        //given
        String imie = "Jan";
        String nazwisko = "Kowalski";
        String pesel = "71031291455";


        //when


        //then
        Assert.assertEquals(imie, uzytkownik.getImie());
        Assert.assertEquals(nazwisko, uzytkownik.getNazwisko());
        Assert.assertEquals(pesel, uzytkownik.getPesel());
        Assert.assertEquals("KB-11111", uzytkownik.getNumerBibloteczny());
        Assert.assertEquals("KB-11112", uzytkownik2.getNumerBibloteczny());
        Assert.assertEquals("KB-11113", uzytkownik3.getNumerBibloteczny());

    }
}