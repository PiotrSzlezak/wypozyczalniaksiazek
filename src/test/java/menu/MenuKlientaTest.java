package menu;

import ksiazka.Gatunek;
import ksiazka.Ksiazka;
import ksiazka.ListaKsiazek;
import listaWypozyczen.ListaWypozyczen;
import narzedzia.Narzedzia;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import uzytkownik.ListaUzytkownikow;
import uzytkownik.Uzytkownik;

import java.io.*;

public class MenuKlientaTest {

    private static Uzytkownik uzytkownikPiotr = new Uzytkownik("Piotr","Szlęzak","98120266628","mojemail@email.com");
    private static ListaKsiazek listaKsiazek = new ListaKsiazek();
    private static Ksiazka ksiazka1 = new Ksiazka("Książka 1", 1, "ISBN 1", "Jan Kowalski", "Wydawca 1", Gatunek.SIFI, 11);
    private static Ksiazka ksiazka2 = new Ksiazka("Książka 2", 2, "ISBN 2", "Jan Nowak", "Wydawca 2", Gatunek.SIFI, 22);
    private static Ksiazka ksiazka3 = new Ksiazka("Książka 3", 3, "ISBN 3", "Artur Nowakowski", "Wydawca 3", Gatunek.ROMANS, 33);
    private static ListaUzytkownikow listaUzytkownikow = new ListaUzytkownikow();
    private static MenuAdmina menuAdmina;
    private static ListaWypozyczen listaWypozyczen;

    @BeforeEach
    void init () {
        listaKsiazek.dodajKsiazke(ksiazka1);
        listaKsiazek.dodajKsiazke(ksiazka2);
        listaKsiazek.dodajKsiazke(ksiazka3);
        listaUzytkownikow.dodajUzytkowniaka(uzytkownikPiotr);
    }

//    @Test
//    void testMenuShouldBorrowBook() throws IOException {
//        //given
//        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
//        menuAdmina = new MenuAdmina(uzytkownikPiotr, listaKsiazek, listaWypozyczen, listaUzytkownikow);
//        Narzedzia.podmienBufferedReaderDoTestow(bufferedReader);
//        menuAdmina.podmienBufferedReaderDoTestow(bufferedReader);
//
//        //when
//        Mockito.when(bufferedReader.readLine()).thenReturn("\n\r","1","Książka 5","758","aabbcc","ja","ty","sifi","12","100","\n\r");
//        menuAdmina.uruchomMenu();
//
//        //then
//        Assert.assertEquals(103,listaKsiazek.getListaKsiazek().size());
//    }

}
